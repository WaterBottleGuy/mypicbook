import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { UploadsPage } from '../../pages/uploads/uploads';

/**
 * Generated class for the CoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cover',
  templateUrl: 'cover.html',
})
export class CoverPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	console.log("as",navParams.get("covertype"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CoverPage');
  }

  selectCover(type){
  	console.log("type", type);
  	this.navCtrl.push(UploadsPage,{
  		coverType: type
  	});
  }

}
