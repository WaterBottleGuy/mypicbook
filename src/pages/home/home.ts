import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { LoginPage } from '../../pages/login/login';
import { CoverPage } from '../../pages/cover/cover';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public logged;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	
  	var logged = false;
  	
  	if(navParams.get("logged")){
  		console.log("abcd",navParams.get("logged"));
  		logged = navParams.get("logged");
  	}

  	if(!logged){
  		this.navCtrl.setRoot(LoginPage);
  	}
  }

  setType(type){
  	console.log(type);
  	this.navCtrl.push(CoverPage,{
  		covertype: type
  	});
  }
}
